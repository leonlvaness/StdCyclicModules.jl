# StdCyclicModules.jl

## Installation

```julia
julia> using Pkg
julia> Pkg.add(url="https://git.rwth-aachen.de/leonlvaness/StdCyclicModules.jl.git")
julia> using StdCyclicModules
```

## Example usage

```julia
  julia> U = UEA("G", 2)
  UEA(GAP: <root system of rank 2>, [[2, -1], [-3, 2]], [[2, -1], [-3, 2], [-1, 1], [1, 0], [3, -1], [0, 1]], 2, 6, Singular G-Algebra (QQ),(f_1,f_2,f_3,f_4,f_5,f_6,h_1,h_2,e_1,e_2,e_3,e_4,e_5,e_6),(dp(14),C), Singular.spluralg{Singular.n_Q}[f_1, f_2, f_3, f_4, f_5, f_6, h_1, h_2, e_1, e_2, e_3, e_4, e_5, e_6])
  
  julia> M = SSCMod(U, [2,1])
  SSCMod(UEA(GAP: <root system of rank 2>, [[2, -1], [-3, 2]], [[2, -1], [-3, 2], [-1, 1], [1, 0], [3, -1], [0, 1]], 2, 6, Singular G-Algebra (QQ),(f_1,f_2,f_3,f_4,f_5,f_6,h_1,h_2,e_1,e_2,e_3,e_4,e_5,e_6),(dp(14),C), Singular.spluralg{Singular.n_Q}[f_1, f_2, f_3, f_4, f_5, f_6, h_1, h_2, e_1, e_2, e_3, e_4, e_5, e_6]), Singular ideal over Singular G-Algebra (QQ),(f_1,f_2,f_3,f_4,f_5,f_6,h_1,h_2,e_1,e_2,e_3,e_4,e_5,e_6),(dp(14),C) with generators (e_1, e_2, e_3, e_4, e_5, e_6, h_1 - 2, h_2 - 1, f_6^3, f_5^2), [2, 1])
  
  julia> P = ssc_mod_polytope(M)
  A polyhedron in ambient dimension 6
  
  julia> Oscar.facets(P)
  13-element SubObjectIterator{AffineHalfspace{fmpq}} over the Halfspaces of R^6 described by:
  x₁ + x₂ + x₃ + x₄ + x₅ ≦ 4
  -x₅ ≦ 0
  -x₄ ≦ 0
  -x₃ ≦ 0
  -x₂ ≦ 0
  x₆ ≦ 2
  x₂ + x₃ + x₆ ≦ 3
  x₅ ≦ 1
  -x₆ ≦ 0
  x₃ + x₄ + x₆ ≦ 3
  -x₁ ≦ 0
  x₂ + x₃ + x₄ + x₅ + x₆ ≦ 4
  x₄ + x₅ + x₆ ≦ 3
  ```
