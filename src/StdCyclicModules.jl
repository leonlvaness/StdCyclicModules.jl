module StdCyclicModules

export UEA, get_uea_vars, SSCMod, ssc_mod_polytope

using Oscar


"""
    sc_table_info(type, n)

Get structure constants of Lie bracket for 
the simple Lie algebra of type `type` and order `n` 
in a way that can be accessed easily.

Credit: Thomas Breuer
"""
function sc_table_info(type::String, n::Int)
    A = GAP.Globals.SimpleLieAlgebra(GAP.GapObj(type), n, GAP.Globals.Rationals)::GAP.GapObj
    B = GAP.Globals.CanonicalBasis(A)::GAP.GapObj
    T = GAP.Globals.StructureConstantsTable(B)::GAP.GapObj
    T = GAP.Globals.ELMS_LIST(T, GAP.GapObj(1:length(B)))::GAP.GapObj
    sct = Matrix{Tuple{Vector{Int}, Vector{Int}}}(T)
    roots = Vector(map(l -> Vector{Int}(GAP.Globals.List(l, x -> GAP.Globals.Position(B, x))::GAP.GapObj),
                       GAP.Globals.ChevalleyBasis(A)::GAP.GapObj))
    return roots, sct
end

#= TODO: Falls die Sortierung nicht gegeben ist, hier sortieren
#V1 = FreeModule(ZZ,deg)
M = MatrixSpace(ZZ, deg, deg)
V = MatrixSpace(ZZ, deg, 1)

# TODO: verifizieren, dass die Wurzeln schon nach Höhe sortiert sind (Issue?)
coeffs = [ solve(M(reduce(hcat,simple_roots)), V(pos_root)) for pos_root in pos_roots ]
=#

"""
Wrapper for all necessary objects to operate on a universal enveloping algebra
for a simple (rational) Lie algebra. Only types for characteristic 0 are supported.
"""
struct UEA
    root_system::GAP.GapObj
    simple_roots::Vector{Vector{Int64}}
    pos_roots::Vector{Vector{Int64}}
    n_simple::Int64 # number of simple roots
    n_pos::Int64 # number of positive roots
    algebra::Singular.PluralRing{Singular.n_Q}
    v::Vector{Singular.spluralg{Singular.n_Q}}

    function UEA(type::String, n::Int64)
        root_indices, sct = sc_table_info(type, n)

        root_system = GAP.Globals.RootSystem(GAP.Globals.SimpleLieAlgebra(GAP.GapObj(type), n, GAP.Globals.Rationals)::GAP.GapObj)
        simple_roots = GAP.gap_to_julia(Vector{Vector{Int64}}, GAP.Globals.SimpleSystem(root_system))
        pos_roots = (GAP.gap_to_julia(Vector{Vector{Int64}}, GAP.Globals.PositiveRoots(root_system)))
        n_simple = length(simple_roots)
        n_pos = length(pos_roots)

        # Translate between indexing
        # GAP: pos - neg - h
        # desired: neg - h - pos
        function desired_to_gap(i)
            n_roots = n_pos
            n_h = n_simple
            if i ≤ n_roots # we want negative
                #return i + n_roots
                return 2*n_roots - i + 1 # ordering_adjusted
            elseif i <= n_roots + n_h # we want h
                return i + n_roots
            else # we want pos
                return i - n_roots - n_h
            end
        end

        function gap_to_desired(i)
            n_roots = n_pos
            n_h = n_simple
            if i ≤ n_roots # we want positive
                return i + n_roots + n_h
            elseif i <= 2*n_roots # we want neg
                #return i - n_roots
                return n_roots - (i - n_roots) + 1 
            else # we want h
                return i - n_roots
            end
        end

        # Set up the base symmetric algebra over the Lie algebra
        n⁻ = "f_".*string.(1:n_pos)
        h = "h_".*string.(1:n_simple)
        n⁺ = "e_".*string.(1:n_pos)
        SA, v = Singular.PolynomialRing(Singular.QQ,[n⁻; h; n⁺], ordering=:degrevlex)

        # Get the structure constants obtained from GAP and transform them to be usable by Singular
        D = [ 
                sct[desired_to_gap(x),desired_to_gap(y)][1] == [] ? 0 : 
                    -sum([sct[desired_to_gap(x),desired_to_gap(y)][2][k] * v[gap_to_desired(sct[desired_to_gap(x),desired_to_gap(y)][1][k])]
                    for k = 1:length(sct[desired_to_gap(x),desired_to_gap(y)][1])]) 
                    for x = 1:length(v), y = 1:length(v)
                
            ] 

        # Set up the universal enveloping algebra
        UA, v = Singular.GAlgebra(SA, 1, Singular.Matrix(SA, D))

        return new(root_system, simple_roots, pos_roots, n_simple, n_pos, UA, v)
    end
end

"""
    get_uea_vars(_UEA)

Get the generators of the given universal enveloping algebra
according to its Cartan decomposition. 

Returns three values `f`, `h` and `e` where 
`f` generates `𝔫⁻`, `h` generates `𝔥` and `e` generates `𝔫⁺`.

# Examples
```jldoctest
julia> U = UEA("A",2)
UEA(GAP: <root system of rank 2>, [[2, -1], [-1, 2]], [[2, -1], [-1, 2], [1, 1]], 2, 3, Singular G-Algebra (QQ),(f_1,f_2,f_3,h_1,h_2,e_1,e_2,e_3),(dp(8),C), Singular.spluralg{Singular.n_Q}[f_1, f_2, f_3, h_1, h_2, e_1, e_2, e_3])

julia> f, h, e = get_uea_vars(U)
(Singular.spluralg{Singular.n_Q}[f_1, f_2, f_3], Singular.spluralg{Singular.n_Q}[h_1, h_2], Singular.spluralg{Singular.n_Q}[e_1, e_2, e_3])

julia> h[2]*f[1]
f_1*h_2 - f_1
```
"""
function get_uea_vars(_UEA::UEA)
    v = _UEA.v
    f = v[1:_UEA.n_pos]
    h = v[(1:_UEA.n_simple) .+ _UEA.n_pos]
    e = v[(1:_UEA.n_pos) .+ (_UEA.n_simple + _UEA.n_pos)]

    return f,h,e
end

"""
Wrapper for a simple standard cyclic module of a universal enveloping algebra
beloging to a simple Lie algebra. For a given maximal weight (as coordinate vector
in fundamental weights), this module is unique up to isomorphism.
"""
struct SSCMod
    ring::UEA
    ideal::Singular.sideal{Singular.spluralg{Singular.n_Q}}
    maximal_weight::Vector{Int64}

    function SSCMod(_UEA::UEA, maximal_weight::Vector{Int64})
        # Set up the ideal that we need to "mod out" for the standard cyclic module
        f, h, e = get_uea_vars(_UEA)
        gens_1 = e;
        gens_2 = [ h[i] - maximal_weight[i] for i=1:_UEA.n_simple ]
        gens_3 = [ f[_UEA.n_simple-i+1 + _UEA.n_pos - _UEA.n_simple]^(maximal_weight[i]+1) for i in 1:_UEA.n_simple ] # TODO This is jank, we need to make sure the f[i] correspond to h[i] above to get the proper module, we need to fix e to get the expected operation, reverting the order here is a quick fix

        return new(_UEA, Singular.Ideal(_UEA.algebra, [ gens_1 ; gens_2; gens_3 ]), maximal_weight)
    end
end

# Caution: This will calculate a Gröbner basis and might be very sloooow
"""
    ssc_mod_polytope(_mod)

Get the polytope, where its lattice points are given by the exponents of elements
of a monomial basis for the module `_mod`, obtained through a Gröbner basis for the
associated ideal.

# Examples
```jldoctest
julia> U = UEA("G", 2)
UEA(GAP: <root system of rank 2>, [[2, -1], [-3, 2]], [[2, -1], [-3, 2], [-1, 1], [1, 0], [3, -1], [0, 1]], 2, 6, Singular G-Algebra (QQ),(f_1,f_2,f_3,f_4,f_5,f_6,h_1,h_2,e_1,e_2,e_3,e_4,e_5,e_6),(dp(14),C), Singular.spluralg{Singular.n_Q}[f_1, f_2, f_3, f_4, f_5, f_6, h_1, h_2, e_1, e_2, e_3, e_4, e_5, e_6])

julia> M = SSCMod(U, [2,1])
SSCMod(UEA(GAP: <root system of rank 2>, [[2, -1], [-3, 2]], [[2, -1], [-3, 2], [-1, 1], [1, 0], [3, -1], [0, 1]], 2, 6, Singular G-Algebra (QQ),(f_1,f_2,f_3,f_4,f_5,f_6,h_1,h_2,e_1,e_2,e_3,e_4,e_5,e_6),(dp(14),C), Singular.spluralg{Singular.n_Q}[f_1, f_2, f_3, f_4, f_5, f_6, h_1, h_2, e_1, e_2, e_3, e_4, e_5, e_6]), Singular ideal over Singular G-Algebra (QQ),(f_1,f_2,f_3,f_4,f_5,f_6,h_1,h_2,e_1,e_2,e_3,e_4,e_5,e_6),(dp(14),C) with generators (e_1, e_2, e_3, e_4, e_5, e_6, h_1 - 2, h_2 - 1, f_6^3, f_5^2), [2, 1])

julia> P = ssc_mod_polytope(M)
A polyhedron in ambient dimension 6

julia> Oscar.facets(P)
13-element SubObjectIterator{AffineHalfspace{fmpq}} over the Halfspaces of R^6 described by:
x₁ + x₂ + x₃ + x₄ + x₅ ≦ 4
-x₅ ≦ 0
-x₄ ≦ 0
-x₃ ≦ 0
-x₂ ≦ 0
x₆ ≦ 2
x₂ + x₃ + x₆ ≦ 3
x₅ ≦ 1
-x₆ ≦ 0
x₃ + x₄ + x₆ ≦ 3
-x₁ ≦ 0
x₂ + x₃ + x₄ + x₅ + x₆ ≦ 4
x₄ + x₅ + x₆ ≦ 3
```
"""
function ssc_mod_polytope(_mod::SSCMod)
    exps = [Singular.leading_exponent_vector(p)[1:_mod.ring.n_pos] for p in Singular.gens(Singular.kbase(Singular.std(_mod.ideal)))]

    # Construct the associated polytope. This should be the FFLV polytope for types A and C
    return convex_hull(mapreduce(permutedims, vcat, exps))
end

end # module